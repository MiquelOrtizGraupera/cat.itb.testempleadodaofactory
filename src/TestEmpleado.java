import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

public class TestEmpleado {
    public static void main(String[] args) throws ParseException {
        DAOFactory db = DAOFactory.getDAOFactory(DAOFactory.POSTGRESQL);

        EmpleadosDAO empDAO = db.getEmpleadoDAO();
//        String date = "2022-05-17";
//        Date dateEmp = new SimpleDateFormat("yyyy/MM/dd").parse(date);

        //Crear Empleado
        Empleado emp = new Empleado(6969,"Koeman","Nada",2,"2022-05-17",4500.00,0.0,10);

        //INSERTAR
        empDAO.insertarEmp(emp);

        //CONSULTAR
        Empleado emp2 = empDAO.consultarEmp(6969);
        System.out.printf("Emp: %s que no hace %s cobra tanto %2f%n",emp2.getApellido(),emp2.getOficio(),emp2.getSalario());

        //MODIFICAR
        emp2.setOficio("Entrenador");
        emp2.setApellido("Hernandez");
        empDAO.modificarEmp(emp2);
        System.out.printf("Emp: %s que es %s es un maquina%n", emp2.getApellido(),emp2.getOficio());

        //ELIMINAR
        empDAO.eliminarEmp(6969);

        //Visualitza les dades del empleat que introdueixes per el teclat
        Scanner sc = new Scanner(System.in);
        int entero = 1;
        while(entero > 0){
            System.out.println("Empleado:");
            entero = sc.nextInt();
            emp = empDAO.consultarEmp(entero);
            System.out.printf("Emp: %s, oficio: %s, dir: %d, fecha de alta: %s, salario: %2f",emp.getApellido()
                    ,emp.getOficio(),emp.getDir(),emp.getFecha_alt(),emp.getSalario());
        }
    }
}
